package com.dxpplusplus.fowallet

import android.os.Bundle
import dxperts.Utils
import dxperts.xmlstring
import kotlinx.android.synthetic.main.activity_about.*

class ActivityAbout : DxpppActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setAutoLayoutContentView(R.layout.activity_about)

        // 设置全屏(隐藏状态栏和虚拟导航栏)
        setFullScreen()

        //  draw version
        val ver = Utils.appVersionName()
        val appname = R.string.kAppName.xmlstring(this)
        label_txt_icon_version.text = "$appname v$ver"
        label_txt_version.text = "$appname v$ver"

        //  back
        layout_back_from_about.setOnClickListener { finish() }
    }
}
